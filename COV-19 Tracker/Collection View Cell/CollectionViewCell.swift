//
//  CollectionViewCell.swift
//  COV-19 Tracker
//
//  Created by Newarpunk on 3/23/20.
//  Copyright © 2020 Akash Stha. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var displayView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var topicLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        displayView.layer.cornerRadius = 5
        
    }
    
}
