//
//  TableViewCell.swift
//  COV-19 Tracker
//
//  Created by Newarpunk on 3/24/20.
//  Copyright © 2020 Akash Stha. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {

    @IBOutlet weak var imageIconView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var boxView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        boxView.setCardRadiusInt(radius: 4)
//        boxView.layer.cornerRadius = 4
        boxView.cardView(radius: 4)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
