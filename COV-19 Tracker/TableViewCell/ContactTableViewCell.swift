//
//  ContactTableViewCell.swift
//  COV-19 Tracker
//
//  Created by Newarpunk on 3/25/20.
//  Copyright © 2020 Akash Stha. All rights reserved.
//

import UIKit

class ContactTableViewCell: UITableViewCell {

    @IBOutlet weak var contactBoxView: UIView!
    @IBOutlet weak var numLabel: UILabel!
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var contactLabel: UILabel!
    
    @IBOutlet weak var btn: UIButton!
    @IBOutlet weak var phoneImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        contactBoxView.setCardRadiusInt(radius: 5)
        contactBoxView.layer.masksToBounds = true
        
        phoneImage.image = phoneImage.image?.withRenderingMode(.alwaysTemplate)
        phoneImage.tintColor = .white
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
