//
//  CountryTableViewCell.swift
//  COV-19 Tracker
//
//  Created by Newarpunk on 3/28/20.
//  Copyright © 2020 Akash Stha. All rights reserved.
//

import UIKit

class CountryTableViewCell: UITableViewCell {

    @IBOutlet weak var countryNameLabel: UILabel!
    @IBOutlet weak var confirmedLabel: UILabel!
    @IBOutlet weak var recoveredLabel: UILabel!
    @IBOutlet weak var deathLabel: UILabel!
    
    @IBOutlet weak var boxView: UIView!
    @IBOutlet weak var flagView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        boxView.layer.cornerRadius = 5
        flagView.layer.cornerRadius = flagView.frame.height / 2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
