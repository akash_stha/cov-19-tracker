//
//  ViewController.swift
//  COV-19 Tracker
//
//  Created by Newarpunk on 3/22/20.
//  Copyright © 2020 Akash Stha. All rights reserved.
//

import UIKit
import Charts

class ViewController: UIViewController {

    @IBOutlet weak var updatedDate: UILabel!
    @IBOutlet weak var updatedTime: UILabel!
    
    @IBOutlet weak var dataPieChart: PieChartView!
    
    @IBOutlet weak var confrimedColor: UIView!
    @IBOutlet weak var recoveredColor: UIView!
    @IBOutlet weak var deathColor: UIView!
    
    @IBOutlet weak var confirmedPercecntage: UILabel!
    @IBOutlet weak var recoveredPercentage: UILabel!
    @IBOutlet weak var deathPercentage: UILabel!
    
    @IBOutlet weak var boxDataView: UIView!
    @IBOutlet weak var recoveredBoxView: UIView!
    @IBOutlet weak var deathBoxView: UIView!
    @IBOutlet weak var confirmedBoxView: UIView!
    @IBOutlet weak var totalBoxView: UIView!
    
    @IBOutlet weak var numRecoveredLabel: UILabel!
    @IBOutlet weak var recoveredIcon: UIView!
    
    @IBOutlet weak var numDeathLabel: UILabel!
    @IBOutlet weak var deathIcon: UIView!
    
    @IBOutlet weak var numConfirmedLabel: UILabel!
    @IBOutlet weak var confirmedIcon: UIView!
    
    @IBOutlet weak var numTotalLabel: UILabel!
    
    var lastUpdate: String = ""
    var splitted = [String]()
    var splittedDate = [String]()
    
    var confirmedData = 0
    var recoveredData = 0
    var deathData = 0
    var totalData = 0
    
    func extractedData(data: CovidDataResponse) {
        lastUpdate = data.lastUpdated!
        splitted = lastUpdate.components(separatedBy: "T")
        splittedDate = splitted[1].components(separatedBy: ".")
        updatedDate.text = splitted[0]
        updatedTime.text = splittedDate[0]
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.showBlurLoader()

        confrimedColor.layer.cornerRadius = 5
        recoveredColor.layer.cornerRadius = 5
        deathColor.layer.cornerRadius = 5
        
//        boxDataView.setCardRadiusInt(radius: 10)
        recoveredBoxView.cardRadius(radius: 5)
        deathBoxView.cardRadius(radius: 5)
        confirmedBoxView.cardRadius(radius: 5)
        totalBoxView.cardRadius(radius: 5)
        
        recoveredIcon.layer.cornerRadius = 3
        deathIcon.layer.cornerRadius = 3
        confirmedIcon.layer.cornerRadius = 3
        
        let viewModel = CovidDataViewModel()
        viewModel.CovidApiDataCall()
        viewModel.setDataListener(data: self)
        
        tabBarItem.selectedImage = UIImage(named: "homeFill")?.withRenderingMode(.alwaysOriginal)

    }
    
    func displayChart(data: CovidDataResponse) {
        dataPieChart.chartDescription?.text = ""
        
        confirmedData = data.totalConfirmed!
        recoveredData = data.totalRecovered!
        deathData = data.totalDeaths!
        totalData = (confirmedData + recoveredData + deathData)
        
        numRecoveredLabel.text = String(recoveredData.withCommas())
        numDeathLabel.text = String(deathData.withCommas())
        numConfirmedLabel.text = String(confirmedData.withCommas())
        numTotalLabel.text = String(totalData.withCommas())

        let perConfirmed = calcpercentage(value: confirmedData)
        let perRecovered = calcpercentage(value: recoveredData)
        let perDeath = calcpercentage(value: deathData)
        
        confirmedPercecntage.text = String(perConfirmed) + "%"
        recoveredPercentage.text = String(perRecovered) + "%"
        deathPercentage.text = String(perDeath) + "%"
        
        let confirmedData = PieChartDataEntry(value: Double(data.totalConfirmed!))
        let recoveredData = PieChartDataEntry(value: Double(data.totalRecovered!))
        let deathData = PieChartDataEntry(value: Double(data.totalDeaths!))
        
        var totalCases = [PieChartDataEntry]()
        
        totalCases = [confirmedData, recoveredData, deathData]
        displayChartData(totalCases: totalCases)
    }
    
    func displayChartData(totalCases: [PieChartDataEntry]) {
        let chartDataSet = PieChartDataSet(entries: totalCases, label: nil)
        let chartData = PieChartData(dataSet: chartDataSet)
        
        let colors = [#colorLiteral(red: 0, green: 0.2384113669, blue: 0.7495505214, alpha: 1), #colorLiteral(red: 0.02520774119, green: 0.7934476733, blue: 0.9939864278, alpha: 1), #colorLiteral(red: 1, green: 0.3597660661, blue: 0.3040229082, alpha: 1)]
        chartDataSet.colors = colors
        chartDataSet.selectionShift = 0
        chartDataSet.drawValuesEnabled = false
        
        dataPieChart.data = chartData
        dataPieChart.legend.enabled = false
        dataPieChart.holeRadiusPercent = 0.68
        dataPieChart.transparentCircleRadiusPercent = 0
    }
    
    func calcpercentage(value: Int) -> Int {
        return ((value * 100) / totalData)
    }

}

extension ViewController: CovidDataApi {
    func getCovidDataApi(logResponse: CovidDataResponse) {
        extractedData(data: logResponse)
        displayChart(data: logResponse)
        view.removeBluerLoader()
    }
}

extension Int {
    func withCommas() -> String {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = NumberFormatter.Style.decimal
        return numberFormatter.string(from: NSNumber(value:self))!
    }
}

