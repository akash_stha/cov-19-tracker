//
//  CountryViewController.swift
//  COV-19 Tracker
//
//  Created by Newarpunk on 3/28/20.
//  Copyright © 2020 Akash Stha. All rights reserved.
//

import UIKit

class CountryViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    var allData = [Areas]()
    var Country = [String]()
    var sortedCountry = [String]()
    func extractedData(data: CovidDataResponse) {
        allData = data.areas!
        for i in 0..<allData.count {
            Country.append((allData[i].displayName)!)
//            sortedCountry = Country.sorted()
            sortedCountry = Country
        }
    }
    
    var searchCountry = [String]()
    var searching: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        
        searchBar.delegate = self
        tableView.delegate = self
        tableView.dataSource = self
        
        searchBar.barTintColor = .blue
        view.showBlurLoader()
        
        let viewModel = CovidDataViewModel()
        viewModel.CovidApiDataCall()
        viewModel.setDataListener(data: self)
    }
    
}

extension CountryViewController: UITableViewDelegate, UITableViewDataSource, CovidDataApi {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searching == true {
            return searchCountry.count
        } else {
            return sortedCountry.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "countryCell", for: indexPath) as! CountryTableViewCell
        let model = sortedCountry[indexPath.item]
        let allModel = allData[indexPath.item]
        if searching == true {
            let searchModel = searchCountry[indexPath.item]
            cell.countryNameLabel.text = searchModel
        } else {
            cell.countryNameLabel.text = model
            cell.confirmedLabel.text = String(allModel.getTotalConfirm())
            cell.recoveredLabel.text = String(allModel.getTotalRecovered())
            cell.deathLabel.text = String(allModel.getTotalDeath())
        }
        return cell
    }
    
    func getCovidDataApi(logResponse: CovidDataResponse) {
        extractedData(data: logResponse)
        tableView.reloadData()
        view.removeBluerLoader()
    }

}

extension CountryViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchCountry = sortedCountry.filter({ $0.prefix(searchText.count) == searchText })
        searching = true
        tableView.reloadData()
    }
    
//    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
//        searching = false
//        searchBar.text = ""
//        tableView.reloadData()
//    }
}
