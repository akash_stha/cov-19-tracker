//
//  ContactViewController.swift
//  COV-19 Tracker
//
//  Created by Newarpunk on 3/25/20.
//  Copyright © 2020 Akash Stha. All rights reserved.
//

import UIKit

class ContactViewController: UIViewController {

    @IBOutlet weak var bar1: UIView!
    @IBOutlet weak var bar2: UIView!
    @IBOutlet weak var bar3: UIView!
    
    @IBOutlet weak var oneProvinceButton: UIButton!
    @IBOutlet weak var twoProvinceButton: UIButton!
    @IBOutlet weak var threeProvinceButton: UIButton!
    
    @IBOutlet weak var contactTableView: UITableView!
    @IBOutlet weak var noDataView: UIView!
    
    enum buttonCase {
        case one
        case two
        case three
    }
    
    var contactData = ContactDataArray()
    
    fileprivate let application = UIApplication.shared
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        contactTableView.delegate = self
        contactTableView.dataSource = self
        
        bar1.layer.cornerRadius = bar1.frame.height / 2
        bar2.layer.cornerRadius = bar1.frame.height / 2
        bar3.layer.cornerRadius = bar1.frame.height / 2
        
//        let tabImage = self.tabBarItem
//        tabImage?.image = UIImage(named: "more")?.withRenderingMode(.alwaysOriginal)
        
//        self.tabBarController?.tabBar.barTintColor = .blue
//        self.tabBarController?.tabBar.tintColor = .blue
        
//        UITabBar.appearance().tintColor = .blue
//        tabBarItem.image = UIImage(named: "contact")?.withRenderingMode(.alwaysOriginal)
        
        tabBarItem.selectedImage = UIImage(named: "contactFill")?.withRenderingMode(.alwaysOriginal)
        
        oneProvinceButton.layer.cornerRadius = 5
        twoProvinceButton.layer.cornerRadius = 5
        threeProvinceButton.layer.cornerRadius = 5
        
        setClickedButtonColor(buttonType: .one)
        noDataView.isHidden = true
    }
    
    @IBAction func phoneClicked(_ sender: UIButton) {
        callAlert(indexPath: sender.tag)
//        openCallApp()
//        openSetting()
    }
    
    func callAlert(indexPath: Int) {
        let model = contactData.dataList[indexPath]
        
        let alert = UIAlertController(title: "Confirm", message: "Do you want to call \(model.number)?", preferredStyle: .alert)
        let okAlert = UIAlertAction(title: "Call", style: .default) { (action: UIAlertAction) in
            print("Calling...\(model.number)...")
            self.openSetting()
        }
        let cancelAlert = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        alert.addAction(okAlert)
        alert.addAction(cancelAlert)
        self.present(alert, animated: true, completion:nil)
    }
    
    func openCallApp() {
        if let phoneURL = URL(string: "tel://0123456789") {
            if application.canOpenURL(phoneURL) {
                application.open(phoneURL, options: [:], completionHandler: nil)
            } else {
                print("error here")
            }
        }

    }
    
    func openSetting() {
        if let settingURL = URL(string: UIApplication.openSettingsURLString) {
            application.open(settingURL, options: [:], completionHandler: nil)
        }
    }
    
    @IBAction func oneProvinceClicked(_ sender: UIButton) {
        oneProvinceButton.isEnabled = false
        twoProvinceButton.isEnabled = true
        threeProvinceButton.isEnabled = true
        setClickedButtonColor(buttonType: .one)
        noDataView.isHidden = true
        print("Province one")
    }
    
    @IBAction func twoProvinceClicked(_ sender: UIButton) {
        oneProvinceButton.isEnabled = true
        twoProvinceButton.isEnabled = false
        threeProvinceButton.isEnabled = true
        setClickedButtonColor(buttonType: .two)
        noDataView.isHidden = false
        print("province two")
    }
    
    @IBAction func threeProvinceClicked(_ sender: UIButton) {
        oneProvinceButton.isEnabled = true
        twoProvinceButton.isEnabled = true
        threeProvinceButton.isEnabled = false
        setClickedButtonColor(buttonType: .three)
        noDataView.isHidden = true
        print("province three")
    }
    
    func setClickedButtonColor(buttonType: buttonCase) {
        switch buttonType {
        case .one:
            oneProvinceButton.backgroundColor = .blue
            oneProvinceButton.setTitleColor(.white, for: .normal)
            twoProvinceButton.backgroundColor = .white
            twoProvinceButton.setTitleColor(.black, for: .normal)
            twoProvinceButton.setCardRadiusInt(radius: 5)
            threeProvinceButton.backgroundColor = .white
            threeProvinceButton.setTitleColor(.black, for: .normal)
            threeProvinceButton.setCardRadiusInt(radius: 5)
            break
        case .two:
            oneProvinceButton.backgroundColor = .white
            oneProvinceButton.setTitleColor(.black, for: .normal)
            oneProvinceButton.setCardRadiusInt(radius: 5)
            twoProvinceButton.backgroundColor = .blue
            twoProvinceButton.setTitleColor(.white, for: .normal)
            threeProvinceButton.backgroundColor = .white
            threeProvinceButton.setTitleColor(.black, for: .normal)
            threeProvinceButton.setCardRadiusInt(radius: 5)
            break
        case .three:
            oneProvinceButton.backgroundColor = .white
            oneProvinceButton.setTitleColor(.black, for: .normal)
            oneProvinceButton.setCardRadiusInt(radius: 5)
            twoProvinceButton.backgroundColor = .white
            twoProvinceButton.setTitleColor(.black, for: .normal)
            twoProvinceButton.setCardRadiusInt(radius: 5)
            threeProvinceButton.backgroundColor = .blue
            threeProvinceButton.setTitleColor(.white, for: .normal)
            break
        }
    }
   
}

extension ContactViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contactData.dataList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "contactCell", for: indexPath) as! ContactTableViewCell
        let model = contactData.dataList[indexPath.item]
        cell.numLabel.text = String(indexPath.item + 1)
        cell.nameLabel.text = model.name
        cell.contactLabel.text = String(model.number)
        cell.btn.tag = indexPath.item
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let model = contactData.dataList[indexPath.item]
        UIApplication.shared.openURL(NSURL(string: "tel://\(model.number)")! as URL)
        print("i am touched")
    }
    
}
