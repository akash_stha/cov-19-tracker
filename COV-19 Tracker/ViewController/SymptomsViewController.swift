//
//  SymptomsViewController.swift
//  COV-19 Tracker
//
//  Created by Newarpunk on 3/24/20.
//  Copyright © 2020 Akash Stha. All rights reserved.
//

import UIKit

class SymptomsViewController: UIViewController {

    @IBOutlet weak var childView: UIView!
    @IBOutlet weak var mainView: UIView!
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var descriptionLabel: UILabel!
    
    var symptomList = SymptomsArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        noNavBar()
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        childView.isHidden = true
        mainView.isHidden = false
        
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    func noNavBar() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for:.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.layoutIfNeeded()
    }

}

extension SymptomsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return symptomList.symptomData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "tableCell", for: indexPath) as! TableViewCell
        let model = symptomList.symptomData[indexPath.item]
        cell.nameLabel.text = model.topic
        cell.imageIconView.image = model.imageName
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.item == 0 {
            hideMainView()
            descriptionLabel.text = "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English."
        } else if indexPath.item == 1 {
            hideMainView()
            descriptionLabel.text = "This is cough section"
        } else if indexPath.item == 2 {
            hideMainView()
            descriptionLabel.text = "This is difficulty breathing section"
        } else if indexPath.item == 3 {
            hideMainView()
            descriptionLabel.text = "This is muscle pain section"
        } else {
            hideMainView()
            descriptionLabel.text = "This is tiring section"
        }
    }
    
    private func hideMainView() {
        mainView.isHidden = true
        childView.isHidden = false
    }
    
}
