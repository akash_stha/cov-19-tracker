//
//  SecondViewController.swift
//  COV-19 Tracker
//
//  Created by Newarpunk on 3/23/20.
//  Copyright © 2020 Akash Stha. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {

    @IBOutlet weak var maskView: UIView!
    @IBOutlet weak var glovesView: UIView!
    @IBOutlet weak var sanitizerView: UIView!
    @IBOutlet weak var soapView: UIView!
    
    @IBOutlet weak var profileImage: UIImageView!
    
    @IBOutlet weak var bar1: UIView!
    @IBOutlet weak var bar2: UIView!
    @IBOutlet weak var bar3: UIView!
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var data = DataArray()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
//        tabBarItem.selectedImage = UIImage(named: "infoFill")?.withRenderingMode(.alwaysOriginal)
//        tabBarItem.image = UIImage(named: "infoFill")
//        navigationController?.tabBarController?.tabBar.selectionIndicatorImage = UIImage(named: "infoFill")?.withRenderingMode(.alwaysOriginal)
        noNavBar()
        
        self.navigationController?.navigationBar.tintColor = UIColor.white;
        maskView.layer.cornerRadius = 40
        glovesView.layer.cornerRadius = 40
        sanitizerView.layer.cornerRadius = 40
        soapView.layer.cornerRadius = 40
        
        profileImage.layer.cornerRadius = 25
        
        bar1.layer.cornerRadius = 1.5
        bar2.layer.cornerRadius = 1.5
        bar3.layer.cornerRadius = 1.5
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
    }
    
    func noNavBar() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for:.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.layoutIfNeeded()
    }

}

extension SecondViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data.dataList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CollectionViewCell
        let model = data.dataList[indexPath.item]
        cell.imageView.image = model.imageName
        cell.topicLabel.text = model.topic
        cell.descriptionLabel.text = model.description
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        if indexPath.item == 0 {
            let vc = UIStoryboard(name: "Symptoms", bundle: nil).instantiateViewController(withIdentifier: "SymptomsViewController") as! SymptomsViewController
            self.navigationController?.pushViewController(vc, animated: true)
//            self.present(UINavigationController(rootViewController: vc), animated: true, completion: nil)
        } else if indexPath.item == 1 {
            print("prevention")
        } else if indexPath.item == 2 {
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController()
            self.navigationController?.pushViewController(vc!, animated: true)
            print("reports")
        } else {
            let vc = UIStoryboard(name: "CountryStoryboard", bundle: nil).instantiateViewController(withIdentifier: "CountryViewController") as! CountryViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
}
