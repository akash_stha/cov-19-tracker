//
//  SymptomsArray.swift
//  COV-19 Tracker
//
//  Created by Newarpunk on 3/24/20.
//  Copyright © 2020 Akash Stha. All rights reserved.
//

import Foundation
import UIKit

class SymptomsArray {
    
    var symptomData = [DataModel]()
    
    init() {
        symptomData.append(DataModel(topic: "Fever", imageName: #imageLiteral(resourceName: "fever"), description: ""))
        symptomData.append(DataModel(topic: "Cough", imageName: #imageLiteral(resourceName: "cough"), description: ""))
        symptomData.append(DataModel(topic: "Difficulty Breathing", imageName: #imageLiteral(resourceName: "virus"), description: ""))
        symptomData.append(DataModel(topic: "Muscle Pain", imageName: #imageLiteral(resourceName: "pain"), description: ""))
        symptomData.append(DataModel(topic: "Tiredness", imageName: #imageLiteral(resourceName: "sleep"), description: ""))
    }
    
}
