//
//  DataArray.swift
//  COV-19 Tracker
//
//  Created by Newarpunk on 3/24/20.
//  Copyright © 2020 Akash Stha. All rights reserved.
//

import Foundation
import UIKit

class DataArray {
    
    var dataList = [DataModel]()
    
    init() {
        dataList.append(DataModel(topic: "Symptoms", imageName: #imageLiteral(resourceName: "cough"), description: "Signs identify the risk of infection"))
        dataList.append(DataModel(topic: "Prevention", imageName: #imageLiteral(resourceName: "bacteria"), description: "Help you avoid the risk of infection"))
        dataList.append(DataModel(topic: "Reports", imageName: #imageLiteral(resourceName: "report"), description: "Data related to the disease"))
        dataList.append(DataModel(topic: "Countries", imageName: #imageLiteral(resourceName: "world"), description: "Infected countries by COVD-19"))
    }
    
}
