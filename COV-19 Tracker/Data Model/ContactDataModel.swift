//
//  ContactDataModel.swift
//  COV-19 Tracker
//
//  Created by Newarpunk on 3/25/20.
//  Copyright © 2020 Akash Stha. All rights reserved.
//

import Foundation

class ContactDataModel {
    
    var name: String
    var number: String
    
    init(name: String, number: String) {
        self.name = name
        self.number = number
    }
    
}
