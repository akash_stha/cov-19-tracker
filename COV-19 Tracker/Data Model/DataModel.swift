//
//  DataModel.swift
//  COV-19 Tracker
//
//  Created by Newarpunk on 3/24/20.
//  Copyright © 2020 Akash Stha. All rights reserved.
//

import Foundation
import UIKit

class DataModel {
    
    var topic: String
    var imageName: UIImage
    var description: String
    
    init(topic: String, imageName: UIImage, description: String) {
        self.topic = topic
        self.imageName = imageName
        self.description = description
    }
    
}
