//
//  ContactDataArray.swift
//  COV-19 Tracker
//
//  Created by Newarpunk on 3/25/20.
//  Copyright © 2020 Akash Stha. All rights reserved.
//

import Foundation

class ContactDataArray {
    
    var dataList = [ContactDataModel]()
    
    init() {
        dataList.append(ContactDataModel(name: "Kritipur Hospital", number: "0-14331390"))
        dataList.append(ContactDataModel(name: "Sukraraj Tropical Hospital", number: "01-4253395"))
        dataList.append(ContactDataModel(name: "Bir Hospital", number: "01-4221119"))
        dataList.append(ContactDataModel(name: "Patan Hospital", number: "01-5522295"))
        dataList.append(ContactDataModel(name: "Alka Hospital", number: "01-5555555"))
        dataList.append(ContactDataModel(name: "B&B Hospital", number: "01-5531933"))
        dataList.append(ContactDataModel(name: "Kist Medical College", number: "01-5201682"))
        dataList.append(ContactDataModel(name: "Nidan Hospital", number: "01-5531333"))
        dataList.append(ContactDataModel(name: "Sumeru Hospital", number: "01-5275333"))
    }
    
}
