//
//  CovidDataViewModel.swift
//  COV-19 Tracker
//
//  Created by Newarpunk on 3/22/20.
//  Copyright © 2020 Akash Stha. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper

class CovidDataViewModel {
    
    var data: CovidDataApi!
    init() {
    }
    
    public func setDataListener(data: CovidDataApi) {
        self.data = data
    }
    
    public func CovidApiDataCall() {
        
        let url = "https://www.bing.com/covid/data"
        Alamofire.request(url, method: .get)
            .responseObject {(response: DataResponse<CovidDataResponse>) in
                switch response.result {
                case .success:
                    let responseObject = response.result.value
                    self.data.getCovidDataApi(logResponse: responseObject!)
                case .failure(let error):
                    print(error.localizedDescription)
            }
        }
    }
}

protocol CovidDataApi {
    func getCovidDataApi(logResponse: CovidDataResponse)
}
